<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>RetailEYE</label>
    <tab>standard-Account</tab>
    <tab>standard-Asset</tab>
    <tab>standard-Case</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Tracking__c</tab>
    <tab>standard-File</tab>
    <tab>Position__c</tab>
    <tab>standard-Document</tab>
    <tab>SKU__c</tab>
</CustomApplication>
